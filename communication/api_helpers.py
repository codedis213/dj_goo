from rest_framework.viewsets import ViewSet

from communication.gql_communication_constants import GQL_POST_REVIEW, \
    GQL_POST_COMMENT, GQL_POST_REPLY
from core.schema import schema


class ReviewCommentsHelper(ViewSet):
    """
    Review Comment ViewSet helper
    """

    @staticmethod
    def post_review_gql(request, *args, **kwargs):
        """
        create review for the given user id, review nd rating
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user_id = request.POST.get("user_id")
        message = request.POST.get("message")
        rating = request.POST.get("rating")
        product_id = int(request.POST.get("product_id"))

        mutation_post_review = GQL_POST_REVIEW % {
            "comment": message,
            "product_id": product_id,
            "rating": rating,
            "user_id": user_id
        }

        post_review_obj = schema.execute(mutation_post_review)
        return post_review_obj.data['createReview']

    @staticmethod
    def post_comment_gql(request, *args, **kwargs):
        """
        create comments for the given user id and comment
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user_id = request.POST.get("user_id")
        message = request.POST.get("message")
        product_id = int(request.POST.get("product_id"))

        mutation_post_comment = GQL_POST_COMMENT % {
            "comment": message,
            "product_id": product_id,
            "user_id": user_id
        }

        post_comment_obj = schema.execute(mutation_post_comment)
        return post_comment_obj.data['createComment']

    @staticmethod
    def post_reply_gql(request, *args, **kwargs):
        """
        post reply for the given parent Comment Id, reply from,
        reply to and reply text
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        parent_comment_id = int(request.POST.get("parentCommentId"))
        reply_from_user_id = int(request.POST.get("replyFrom"))
        # reply_to_user_id = int(request.POST.get("replyTo"))
        message = request.POST.get("replyText")
        product_id = int(request.POST.get("productId"))

        mutation_post_reply = GQL_POST_REPLY % {
            "comment_id": parent_comment_id,
            "reply_text": message,
            "user_id": reply_from_user_id
        }

        post_reply_obj = schema.execute(mutation_post_reply)
        return post_reply_obj.data['createReply']
