import graphene
from graphene_django.types import DjangoObjectType

from account.models import User
from communication.models import Review, Comment, Reply
from product.models import Product


class CommentType(DjangoObjectType):
    """
    GraphQl Schema for the model Comment
    """
    class Meta:
        """
        Meta for GraphQl Schema for the model Comment
        """
        model = Comment


class CreateComment(graphene.Mutation):
    """
    GraphQl Mutation for GraphQl Schema  the below
    - CommentType
    """
    comment = graphene.Field(CommentType)

    class Arguments:
        """
        argument for the comment mutation
        """
        product_id = graphene.Int()
        user_id = graphene.Int()
        comment_text = graphene.String()

    def mutate(self, info, product_id, user_id, comment_text):
        """

        :param info:
        :param product_id:
        :param user_id:
        :param comment_text:
        :return:
        """
        product_obj = Product.objects.get(id=product_id)
        user_obj = User.objects.get(id=user_id)
        comment_obj = Comment(
            product_id=product_obj,
            user_id=user_obj,
            comment_text=comment_text
        )
        comment_obj.save()

        return CreateComment(
            comment=comment_obj
        )


class ReplyType(DjangoObjectType):
    """
    GraphQl Schema for the model Reply
    """
    class Meta:
        """
        Meta for GraphQl Schema for the model Reply
        """
        model = Reply


class CreateReply(graphene.Mutation):
    """
    GraphQl mutation for the Reply
    """
    reply = graphene.Field(ReplyType)

    class Arguments:
        """
        argument for the reply mutation
        """
        comment_id = graphene.Int()
        user_id = graphene.Int()
        reply_text = graphene.String()

    def mutate(self, info, comment_id, user_id, reply_text):
        """

        :param info:
        :param comment_id:
        :param user_id:
        :param reply_text:
        :return:
        """
        comment_obj = Comment.objects.get(id=comment_id)
        user_obj = User.objects.get(id=user_id)
        reply_obj = Reply(
            comment=comment_obj,
            user=user_obj,
            reply_text=reply_text
        )
        reply_obj.save()

        return CreateReply(
            reply=reply_obj
        )


class ReviewType(DjangoObjectType):
    """
    GraphQl Schema for the model Review
    """

    class Meta:
        """
        Meta for GraphQl Schema for the model Review
        """
        model = Review


class CreateReview(graphene.Mutation):
    """
    GraphQl mutation for the review
    """
    review = graphene.Field(ReviewType)

    class Arguments:
        """
        arguments for the Create Review Mutation
        """
        product_id = graphene.Int()
        user_id = graphene.Int()
        rating = graphene.Int()
        comment = graphene.String()

    def mutate(self, info, product_id, user_id, rating, comment):
        """

        :param info:
        :param product_id:
        :param user_id:
        :param rating:
        :param comment:
        :return:
        """
        product_obj = Product.objects.get(id=product_id)
        user_obj = User.objects.get(id=user_id)
        review_obj = Review(
            product_id=product_obj,
            user_id=user_obj,
            rating=rating,
            comment_text=comment
        )
        review_obj.save()

        return CreateReview(
            review=review_obj
        )


class Query:
    """
    GraphQl Query for the comment, review, reply
    """
    all_comments = graphene.List(CommentType)
    all_reviews = graphene.List(ReviewType)
    all_replies = graphene.List(ReplyType)

    def resolve_all_comments(self, info, **kwargs):
        """

        :param info:
        :param kwargs:
        :return:
        """
        comment_obj_list = Comment.objects.all()
        return comment_obj_list

    def resolve_all_replies(self, info, **kwargs):
        """

        :param info:
        :param kwargs:
        :return:
        """
        cat_obj = Reply.objects.all()
        return cat_obj

    def resolve_all_reviews(self, info, **kwargs):
        """

        :param info:
        :param kwargs:
        :return:
        """
        review_obj_list = Review.objects.all()
        return review_obj_list


class Mutation(graphene.ObjectType):
    """
    GraphQl mutation for comment, reply, review
    """
    create_comment = CreateComment.Field()
    create_reply = CreateReply.Field()
    create_review = CreateReview.Field()
