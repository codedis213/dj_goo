from django.contrib import admin

from .models import Reply, Review, Comment, Notification, ComplaintChat, \
    Complaint


class ReplyInline(admin.TabularInline):
    """
    TabularInline Admin form for Reply
    """
    model = Reply
    extra = 1


class ReplyAdmin(admin.ModelAdmin):
    """
    ModelAdmin for Reply
    """

    def get_model_perms(self, request):
        """

        :param request:
        :return:
        """
        perms = admin.ModelAdmin.get_model_perms(self, request)
        perms['list_hide'] = True
        return perms


class ComplaintChatInline(admin.TabularInline):
    """
    TabularInline Admin form for ComplaintChat
    """
    model = ComplaintChat
    extra = 1


class ComplaintChatAdmin(admin.ModelAdmin):
    """
    Model Admin form for ComplaintChat
    """

    def get_model_perms(self, request):
        perms = admin.ModelAdmin.get_model_perms(self, request)
        perms['list_hide'] = True
        return perms


class ComplaintAdmin(admin.ModelAdmin):
    """
    Model Admin form for Complaint
    """
    inlines = (ComplaintChatInline,)


class ReviewAdmin(admin.ModelAdmin):
    """
    Model Admin form for Review
    """
    autocomplete_fields = ['product_id']


class CommentAdmin(admin.ModelAdmin):
    """
    Model Admin form for Comment
    """
    autocomplete_fields = ['product_id']
    inlines = (ReplyInline,)


admin.site.register(Review, ReviewAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Reply, ReplyAdmin)

admin.site.register(Notification)
admin.site.register(ComplaintChat, ComplaintChatAdmin)
admin.site.register(Complaint, ComplaintAdmin)
