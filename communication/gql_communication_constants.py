GQL_POST_REVIEW = """
mutation{
    createReview(
        comment: "%(comment)s", 
        productId: %(product_id)s, 
        rating: %(rating)s, 
        userId: %(user_id)s
    ){
        review{
            id
            userId{
                id
                username
            }
            commentText
            createdDate
            rating
            likedCount
            userLiked{
                id
                username
            }
            dislikeCount
            userDisliked{
                id
                username
            }
        }
    }
}
"""

GQL_POST_COMMENT = """
mutation{
    createComment(
        commentText: "%(comment)s", 
        productId: %(product_id)s, 
        userId: %(user_id)s
    ){
         comment {
          id
          commentText
          userId{
            id
            username
          }
          createdDate
          replySet{
            comment{
              id
              userId{
                id
                username
              } 
            }
            replyText
            replyTo{
              id
              username
            }
          }
        }
    }
}
"""

GQL_POST_REPLY = """
mutation {
  createReply(
  commentId: %(comment_id)s, 
  replyText: "%(reply_text)s", 
  userId: %(user_id)s){
    reply {
      id
      user {
        id
        username
      }
      comment {
        id
        userId {
          id
          username
        }
        commentText
      }
      replyTo {
        id
        username
      }
      CREATEDDate
      replyText
    }
  }
}

"""

if __name__ == "__main__":
    MUTATION_GQL_POST_REVIEW = GQL_POST_REVIEW % {
        "comment": "This is my first comment",
        "product_id": 839,
        "rating": 4,
        "user_id": 3593}
    print("#" * 20)
    print("post review")
    print(MUTATION_GQL_POST_REVIEW)

    MUTATION_GQL_POST_COMMENT = GQL_POST_COMMENT % {
        "comment": "This is my first comment",
        "product_id": 839,
        "user_id": 3593}
    print("#" * 20)
    print("post comment")
    print(MUTATION_GQL_POST_COMMENT)

    MUTATION_GQL_POST_REPLY = GQL_POST_REPLY % {
        "comment_id": "This is my first comment",
        "reply_text": 839,
        "user_id": 3593}
    print("#" * 20)
    print("post reply")
    print(MUTATION_GQL_POST_COMMENT)
