from djongo import models

from account.models import User
from product.models import Product


class Comment(models.Model):
    """
    Model to store the Comment
    """
    product_id = models.ForeignKey(Product,
                                   related_name="comment_on_product",
                                   on_delete=models.DO_NOTHING)
    user_id = models.ForeignKey(User, related_name="comment_by_user",
                                on_delete=models.DO_NOTHING)
    comment_text = models.TextField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        """
        Meta for Comment model
        """
        ordering = ('-created_date',)


class Reply(models.Model):
    """
    Model to store the Reply
    """
    comment = models.ForeignKey("Comment",
                                on_delete=models.DO_NOTHING)
    user = models.ForeignKey(User, related_name="user_reply_on_comment",
                             on_delete=models.DO_NOTHING,
                             null=True)
    reply_text = models.TextField(null=True, blank=True)
    reply_to = models.ManyToManyField(User,
                                      related_name="user_reply_to",
                                      )
    CREATED_DATE = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)


class Review(models.Model):
    """
    Model to store the Review
    """
    product_id = models.ForeignKey(Product,
                                   related_name="review_on_product",
                                   on_delete=models.DO_NOTHING)
    user_id = models.ForeignKey(User, related_name="review_by_user",
                                on_delete=models.DO_NOTHING)
    comment_text = models.TextField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    rating = models.IntegerField(null=True, blank=True)
    liked_count = models.IntegerField(null=True, blank=True)
    user_liked = models.ManyToManyField(
        User, related_name="user_like_review", blank=True)
    dislike_count = models.IntegerField(null=True, blank=True)
    user_disliked = models.ManyToManyField(
        User, related_name="user_dislike_review", blank=True)

    class Meta:
        """
        Meta for Review model
        """
        ordering = ('-created_date',)


class Notification(models.Model):
    """
    Model to store the Notification
    """
    sender = models.ForeignKey(User, related_name="sender_of_notification",
                               on_delete=models.DO_NOTHING)
    receiver = models.ManyToManyField(User,
                                      related_name="receiver_of_notification")
    datetime = models.DateTimeField(auto_now_add=True)
    message = models.TextField()


class Complaint(models.Model):
    """
    Model to store the Complaint
    """
    product_id = models.ForeignKey(
        Product, related_name="complaint_on_product",
        on_delete=models.DO_NOTHING)
    user_id = models.ForeignKey(User, related_name="complaint_by_user",
                                on_delete=models.DO_NOTHING)
    comment = models.TextField()
    complaint_stage = models.CharField(max_length=15)


class ComplaintChat(models.Model):
    """
    Model to store the Complaint Chats
    """
    complaint = models.ForeignKey("Complaint", on_delete=models.CASCADE)
    user_id = models.ForeignKey(
        User, related_name="complaint_reply_by_user",
        on_delete=models.DO_NOTHING)
    comment = models.TextField()
