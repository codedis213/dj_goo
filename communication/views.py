from django.shortcuts import render

from .api_helpers import ReviewCommentsHelper


# Create your views here.

class ProductReviewComments(ReviewCommentsHelper):
    """
    ViewSet for the Product Review and Comments
    """

    def post_review(self, request, *args, **kwargs):
        """
        post review to db
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        post_review_objs = self.post_review_gql(request=request,
                                                args=args,
                                                kwargs=kwargs)

        context = {
            "review": post_review_objs["review"]
        }

        return render(request, 'core/winter/review_item.html', context)

    def post_comment(self, request, *args, **kwargs):
        """
        post comment into db
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        post_comments_objs = self.post_comment_gql(request=request,
                                                   args=args,
                                                   kwargs=kwargs)

        context = {
            "comment_obj": post_comments_objs["comment"],
            "parent_comment_id": post_comments_objs["comment"]["id"],
            "product_id": int(request.POST.get("product_id"))
        }

        return render(request, 'core/winter/comment_item.html', context)

    def post_reply(self, request, *args, **kwargs):
        """
        post reply into db
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        post_reply_objs = self.post_reply_gql(request=request,
                                              args=args,
                                              kwargs=kwargs)

        context = {
            "reply": post_reply_objs["reply"],
            "parent_comment_id": int(request.POST.get("parentCommentId")),
            "product_id": int(request.POST.get("productId"))
        }

        return render(request, 'core/winter/reply_lists.html', context)
