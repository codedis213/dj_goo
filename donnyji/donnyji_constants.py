ORDER_TYPE_CHOICES = (
    (1, "COD"),
    (2, 'Online'),
    (3, 'Visit Shop')
)

CANCEL_OR_RETURN_CHOICE = (
    (1, "No"),
    (2, "Canceled"),
    (3, 'Returned'),
)

TYPE_OF_PAYMENT_CHOICE = [
    ('COD', 'COD'),
    ('ONLINE', 'ONLINE'),
    ('SHOP VISIT', 'SHOP VISIT'),
]

DISCOUNT_TYPE = (
    (1, "Percentage"),
    (2, "Flat")
)

VOUCHER_TYPE = (
    (1, "Discount"),
    (2, "Sale")
)
