from django.contrib import admin

from .models import CodOrderTrack, \
    OnlineOrderTrack, ShopVisitOrderTrack, OrderTrack, CancelCodOrderTrack, \
    ReturnCodOrderTrack, CancelOnlineOrderTrack, ReturnOnlineOrderTrack, \
    CancelShopVisitOrderTrack, ReturnShopVisitOrderTrack, OrderStageTrack


class CodOrderTrackInline(admin.StackedInline):
    """
    Stacked Inline Admin form for CodOrderTrack
    """
    model = CodOrderTrack
    extra = 1


class CancelCodOrderTrackInline(admin.StackedInline):
    """
    Stacked Inline Admin form for CancelCodOrderTrack
    """
    model = CancelCodOrderTrack
    extra = 1


class ReturnCodOrderTrackInline(admin.StackedInline):
    """
    Stacked Inline Admin form for ReturnCodOrderTrack
    """
    model = ReturnCodOrderTrack
    extra = 1


class OnlineOrderTrackInline(admin.StackedInline):
    """
    Stacked Inline Admin form for OnlineOrderTrack
    """
    model = OnlineOrderTrack
    extra = 1


class CancelOnlineOrderTrackInline(admin.StackedInline):
    """
    Stacked Inline Admin form for CancelOnlineOrderTrack
    """
    model = CancelOnlineOrderTrack
    extra = 1


class ReturnOnlineOrderTrackInline(admin.StackedInline):
    """
    Stacked Inline Admin form for ReturnOnlineOrderTrack
    """
    model = ReturnOnlineOrderTrack
    extra = 1


class ShopVisitOrderTrackInline(admin.StackedInline):
    """
    Stacked Inline Admin form for ShopVisitOrderTrack
    """
    model = ShopVisitOrderTrack
    extra = 1


class CancelShopVisitOrderTrackInline(admin.StackedInline):
    """
    Stacked Inline Admin form for CancelShopVisitOrderTrack
    """
    model = CancelShopVisitOrderTrack
    extra = 1


class ReturnShopVisitOrderTrackInline(admin.StackedInline):
    """
    Stacked Inline Admin form for ReturnShopVisitOrderTrack
    """
    model = ReturnShopVisitOrderTrack
    extra = 1


class OrderTrackAdmin(admin.ModelAdmin):
    """
    Model Admin for OrderTrack
    """
    inlines = (CodOrderTrackInline, CancelCodOrderTrackInline,
               ReturnCodOrderTrackInline, OnlineOrderTrackInline,
               CancelOnlineOrderTrackInline, ReturnOnlineOrderTrackInline,
               ShopVisitOrderTrackInline, CancelShopVisitOrderTrackInline,
               ReturnShopVisitOrderTrackInline)


class CodOrderTrackAdmin(admin.ModelAdmin):
    """
    Model Admin for OrderTrack
    """

    def get_model_perms(self, request):
        """

        :param args:
        :param kwargs:
        :return:
        """
        perms = admin.ModelAdmin.get_model_perms(self, request)
        perms['list_hide'] = True
        return perms


class OnlineOrderTrackAdmin(admin.ModelAdmin):
    """
   Model Admin for OnlineOrderTrack
   """

    def get_model_perms(self, request):
        """

        :param args:
        :param kwargs:
        :return:
        """
        perms = admin.ModelAdmin.get_model_perms(self, request)
        perms['list_hide'] = True
        return perms


class ShopVisitOrderTrackAdmin(admin.ModelAdmin):
    """
   Model Admin for ShopVisitOrderTrack
   """

    def get_model_perms(self, request):
        """

        :param request:
        :return:
        """

        perms = admin.ModelAdmin.get_model_perms(self, request)
        perms['list_hide'] = True
        return perms


class OrderStageTrackAdmin(admin.ModelAdmin):
    """
   Model Admin for OrderStageTrack
   """

    def get_model_perms(self, request):
        perms = admin.ModelAdmin.get_model_perms(self, request)
        perms['list_hide'] = True
        return perms


admin.site.register(OrderTrack, OrderTrackAdmin)
admin.site.register(CodOrderTrack, CodOrderTrackAdmin)
admin.site.register(OnlineOrderTrack, OnlineOrderTrackAdmin)
admin.site.register(ShopVisitOrderTrack, ShopVisitOrderTrackAdmin)
admin.site.register(OrderStageTrack, OrderStageTrackAdmin)
