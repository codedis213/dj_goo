from djongo import models

from account.models import User
from communication.models import Notification
from product.models import Product


class OrderStageTrack(models.Model):
    """
    OrderStageTrack Model
    """
    date_of_track = models.DateTimeField()
    stage_name = models.CharField(max_length=50)
    stage_message = models.TextField()
    time_of_current_stage = models.DateTimeField()
    time_to_move_at_next_stage = models.DateTimeField()
    stage_notification_message = models.DateTimeField()
    notification_track = models.ForeignKey(
        Notification,
        related_name="ost_notification_track", on_delete=models.CASCADE)


class OrderTrack(models.Model):
    """
    OrderTrack Model
    """

    ordered_bag = models.ForeignKey("order.OrderedBag",
                                    on_delete=models.DO_NOTHING)


class OrderTrackType(models.Model):
    """
    OrderTrackType Model
    """
    order_track = models.ForeignKey("OrderTrack", on_delete=models.DO_NOTHING)
    product_details = models.ManyToManyField("order.ProductOrdered")
    order_stage_track = models.ManyToManyField(OrderStageTrack)

    class Meta:
        """
        Meta for OrderTrackType
        """
        abstract = True


class CodOrderTrack(OrderTrackType):
    """
    CodOrderTrack Model
    """


class CancelCodOrderTrack(CodOrderTrack):
    """
    CancelCodOrderTrack Model
    """


class ReturnCodOrderTrack(CodOrderTrack):
    """
    ReturnCodOrderTrack Model
    """


class OnlineOrderTrack(OrderTrackType):
    """
    OnlineOrderTrack Model
    """


class CancelOnlineOrderTrack(OnlineOrderTrack):
    """
    CancelOnlineOrderTrack Model
    """


class ReturnOnlineOrderTrack(OnlineOrderTrack):
    """
    ReturnOnlineOrderTrack Model
    """


class ShopVisitOrderTrack(OrderTrackType):
    """
    ShopVisitOrderTrack Model
    """
    appoint_fixed_time = models.DateTimeField()


class CancelShopVisitOrderTrack(ShopVisitOrderTrack):
    """
    CancelShopVisitOrderTrack Model
    """


class ReturnShopVisitOrderTrack(ShopVisitOrderTrack):
    """
    ReturnShopVisitOrderTrack Model
    """


class ShopVisit(models.Model):
    """
    ShopVisit Model
    """
    customer_id = models.ForeignKey(
        User, related_name="sv_customer",
        on_delete=models.DO_NOTHING)
    seller_id = models.ForeignKey(
        User, related_name="sv_seller",
        on_delete=models.DO_NOTHING)
    product_id = models.ManyToManyField(
        Product, related_name="sv_product")
    total_price = models.IntegerField()
    appoint_fixed_time = models.DateTimeField()
    notification_track = models.ManyToManyField(
        Notification,
        related_name="sv_notification_track")
