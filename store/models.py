from djongo import models

from account.models import CardDetail
from common.models import Address


class StoreCardDetail(CardDetail):
    """
    Credit/Debit Card Details of Store house
    """
    store_house = models.ForeignKey("StoreHouse",
                                    related_name="store_card_detail",
                                    on_delete=models.CASCADE)
    is_primary = models.BooleanField(default=False)


class StoreHouse(Address):
    """
    Seller Store house
    """
    seller = models.ForeignKey("seller.Seller",
                               related_name="dashboards_seller",
                               on_delete=models.CASCADE)
    shop_unique_code = models.CharField(max_length=20)
    shop_name = models.CharField(max_length=30)
    shop_no = models.CharField(max_length=30)
    lat = models.CharField(max_length=20, blank=True, null=True)
    lon = models.CharField(max_length=20, blank=True, null=True)
