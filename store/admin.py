from django.contrib import admin

from .models import StoreHouse, StoreCardDetail


class StoreCardDetailInline(admin.TabularInline):
    """
    TabularInline Admin form for StoreCardDetail
    """
    model = StoreCardDetail
    extra = 1


class StoreHouseAdmin(admin.ModelAdmin):
    """
    ModelAdmin for StoreHouse
    """
    search_fields = ['seller__username']
    inlines = (StoreCardDetailInline,)


admin.site.register(StoreHouse, StoreHouseAdmin)
