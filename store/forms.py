from django import forms
from .models import StoreHouse


class StoreHouseForm(forms.ModelForm):
    """
    This form is created to customise the store house forms fields in django admin
    for a particular seller.
    """
    shop_unique_code = forms.CharField(widget=forms.TextInput(attrs={'size': '4'}))
    shop_name = forms.CharField(widget=forms.TextInput(attrs={'size': '10'}))
    shop_no = forms.CharField(widget=forms.TextInput(attrs={'size': '5'}))
    lat = forms.CharField(widget=forms.TextInput(attrs={'size': '4'}))
    lon = forms.CharField(widget=forms.TextInput(attrs={'size': '4'}))
    city = forms.CharField(widget=forms.TextInput(attrs={'size': '5'}))
    state = forms.CharField(widget=forms.TextInput(attrs={'size': '5'}))
    country = forms.CharField(widget=forms.TextInput(attrs={'size': '5'}))
    zip = forms.CharField(widget=forms.TextInput(attrs={'size': '4'}))
    area = forms.CharField(widget=forms.TextInput(attrs={'size': '18'}))
    near_by = forms.CharField(widget=forms.TextInput(attrs={'size': '18'}))

    class Meta:
        model = StoreHouse
        fields = ['shop_unique_code', 'shop_name', 'shop_no', 'city', 'state',
                  'country', 'zip', 'area', 'near_by', 'lat', 'lon']
