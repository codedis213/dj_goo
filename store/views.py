# Create your views here.
from .models import StoreHouse
from django.conf import settings
from rest_framework import status
from django.core.cache import cache
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.cache.backends.base import DEFAULT_TIMEOUT

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@csrf_exempt
def get_stores(request):
    """
    This method will get all the nearby stores as per the given radius
    :param request:
    :return:
    """
    longitude = request.GET.get('longitude')
    latitude = request.GET.get('latitude')
    distance = request.GET.get('distance')

    if longitude is None or latitude is None or distance is None:
        return JsonResponse({"status": "longitude, latitude, distance are required fields"},
                            status=status.HTTP_400_BAD_REQUEST)

    # check whether we have records present in the cache for particular lat and long.
    area = {'longitude': longitude,
            'latitude': latitude,
            'distance': distance}

    if area == cache.get('area'):
        result = cache.get('store_house_result')
        if result:
            return JsonResponse(result, safe=False)

    # if records are not present we have to fetch and it in cache
    raw_query = f"""SELECT * FROM ( SELECT *, ( ( ( acos( sin((  28.4961791 * pi() / 180)) * sin(( {latitude} * pi() / 180))
                    + cos(( 28.4961791* pi() /180 )) * cos(( {latitude} * pi() / 180)) * cos((( 77.4353256 - {longitude}) * 
                    pi()/180))) ) * 180/pi() ) * 60 * 1.1515 * 1.609344 ) as distance FROM store_storehouse ) 
                    store_storehouse WHERE distance <= {distance};"""

    stores = StoreHouse.objects.raw(raw_query)
    result = []

    for store in stores:
        store_data = {
            "shop_id": store.id,
            "seller_id": store.seller_id,
            "shop_name": store.shop_name,
            "shop_no": store.shop_no,
        }
        result.append(store_data)
    cache.set('store_house_result', result, timeout=CACHE_TTL)
    cache.set('area', area, timeout=CACHE_TTL)
    return JsonResponse(result, safe=False)
