from django import forms

from common.models import OtherImages, Images
from product.models import Product


class ProductAdminForm(forms.ModelForm):
    """
    Admin Form for ProductAdmin
    """

    class Meta:
        """
        declare Meta for the  Admin Form ProductAdmin
        """
        model = Product
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        """
        super(ProductAdminForm, self).__init__(*args, **kwargs)
        if 'other_images' in self.initial:
            self.fields['other_images'].queryset = OtherImages.objects.filter(
                product__pk=self.instance.pk)


class ProductAvailableForm(forms.ModelForm):
    """
    Admin Form for ProductAvailable
    """

    class Meta:
        """
        declare Metas for ProductAvailable Form
        """
        # model = ProductAvailable
        # fields = "__all__"
        exclude = ("product",)

    def __init__(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        """
        super(ProductAvailableForm, self).__init__(*args, **kwargs)
        if 'images' in self.initial:
            self.fields['images'].queryset = Images.objects.filter(
                product__pk=self.instance.pk)
