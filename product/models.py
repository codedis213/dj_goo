# from django.contrib.postgres.fields import JSONField
from djongo import models

from common.models import Images


class CategorySpec(models.Model):
    """
    Model for CategorySpec
    """
    category = models.ForeignKey("Category", related_name="cs_category",
                                 on_delete=models.CASCADE)
    params_name = models.CharField(max_length=50)


class Category(models.Model):
    """
    Model for Category
    """
    category_name = models.CharField(max_length=50)
    parent_category = models.ForeignKey("self",
                                        related_name="cat_parent_category",
                                        on_delete=models.DO_NOTHING,
                                        null=True, blank=True)

    def __str__(self):
        """

        :return:
        """
        return '%s' % self.category_name


class Brand(models.Model):
    """
    Model for Brand
    """
    category = models.ForeignKey("Category", related_name="brand_category",
                                 on_delete=models.DO_NOTHING)
    brand_name = models.CharField(max_length=20)

    def __str__(self):
        """

        :return:
        """
        return "%s_%s" % (self.category.category_name, self.brand_name)

    def get_brand_with_category(self):
        """

        :return:
        """
        return "%s_%s" % (self.category.category_name, self.brand_name)


class ProductAvailable(models.Model):
    """
    Model for ProductAvailable
    """
    product = models.ForeignKey("Product", related_name="pa_product",
                                on_delete=models.DO_NOTHING,
                                db_constraint=False)
    param_name = models.CharField(max_length=100)
    param_value = models.CharField(max_length=100)
    images = models.ManyToManyField(Images, related_name="pa_images")
    price = models.CharField(max_length=50)
    quantity = models.IntegerField()


class ProductFeatures(models.Model):
    """
    Model for ProductFeatures
    """
    product = models.ForeignKey("Product", related_name="pf_product",
                                on_delete=models.DO_NOTHING,
                                db_constraint=False)
    parameter = models.CharField(max_length=50)
    value = models.CharField(max_length=100)


class ProductImages(models.Model):
    """
    Model for ProductImages
    """
    product = models.ForeignKey("Product", related_name="product_image",
                                on_delete=models.DO_NOTHING,
                                db_constraint=False)
    image = models.ImageField(upload_to='images', null=True,
                              default='profile_image/none/no-img.png')
    image_string = models.URLField(null=True, blank=True)
    is_primary_pic = models.BooleanField(default=False)
    is_secondary_pic = models.BooleanField(default=False)


class Product(models.Model):
    """
    Model for Product
    """
    store_house = models.ForeignKey("store.StoreHouse",
                                    related_name="product_store_house",
                                    on_delete=models.DO_NOTHING, null=True,
                                    blank=True)
    category = models.ForeignKey(Category, related_name="category",
                                 on_delete=models.DO_NOTHING)
    brand = models.ForeignKey(Brand, related_name="brand",
                              on_delete=models.DO_NOTHING, null=True,
                              blank=True)
    sku = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    manufacture_date = models.DateTimeField(null=True, blank=True)
    expire_date = models.DateTimeField(null=True, blank=True)
    descriptions = models.TextField(blank=True, null=True)
    actual_price = models.CharField(max_length=50, null=True, blank=True)
    sale_price = models.CharField(max_length=50, null=True, blank=True)
    quantity = models.IntegerField()
    item_sold = models.IntegerField()
    product_rating = models.CharField(max_length=50, blank=True, null=True)
    returned_item = models.IntegerField()

    class Meta:
        """
        Declare Metas
        """

        def __str__(self):
            return "%s" % self.name
