import graphene
from django.conf import settings
from django.db.models import QuerySet


class PaginatedList(graphene.List):
    """
    Product Pagination
    """
    def __init__(self, *args, **kwargs):
        kwargs['first'] = graphene.Int()
        kwargs['skip'] = graphene.Int()
        super().__init__(*args, **kwargs)

    @staticmethod
    def paginate(qs: QuerySet, skip: int = None, first: int = None) -> QuerySet:
        """

        :param qs:
        :param skip:
        :param first:
        :return:
        """
        max_per_page = settings.GRAPHENE['MAX_PER_PAGE']
        skip = skip or 0
        first = first or max_per_page
        first = first if first < max_per_page else max_per_page
        if skip:
            qs = qs[skip:]

        if first:
            qs = qs[:first]

        return qs
