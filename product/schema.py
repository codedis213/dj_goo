import graphene
from django.db.models import Q, FloatField
from django.db.models.functions import Cast
from graphene_django.types import DjangoObjectType

from product.models import ProductFeatures, Product, Brand, Category, \
    ProductImages


class ProductImagesType(DjangoObjectType):
    """
    GraphQL Schema for ProductImages Model
    """

    class Meta:
        """
        Meta for GraphQL Schema ProductImages
        """
        model = ProductImages


class BrandType(DjangoObjectType):
    """
    GraphQL Schema for  Brand Model

    """

    class Meta:
        """
        Meta for GraphQL Schema Brand
        """
        model = Brand


class ProductFeaturesType(DjangoObjectType):
    """
    GraphQL Schema for ProductFeatures Model
    """

    class Meta:
        """
        define the model to auto create Model Fields
        """
        model = ProductFeatures


class ProductType(DjangoObjectType):
    """
    GraphQL Schema for  Product Model
    """

    class Meta:
        """
        define the model to auto create Model Fields
        """
        model = Product


class CategoryType(DjangoObjectType):
    """
    GraphQL Schema for Category Model
    """

    class Meta:
        """
        define the model to auto create Model Fields
        """
        model = Category


class Query:
    """
    GraphQL Query for Product, Color, Brands, Category, Images
    """
    all_products = graphene.List(ProductType,
                                 id=graphene.Int(),
                                 category_name=graphene.String(),
                                 brand_names=graphene.String(),
                                 color_names=graphene.String(),
                                 min_price=graphene.String(),
                                 max_price=graphene.String())
    all_colors = graphene.List(ProductFeaturesType,
                               category_name=graphene.String(),
                               brand_names=graphene.String(),
                               )
    all_brands = graphene.List(BrandType,
                               category_name=graphene.String())

    all_categories = graphene.List(CategoryType)

    all_images = graphene.List(ProductImagesType)

    def resolve_all_products(self, info, **kwargs):
        """
        get product based on the category_name, color_names, brand_names,
        min_price, max_price etc

        :param info:
        :param kwargs:
        :return:
        """
        q_object = Q()
        product_id = kwargs.get("id", False)
        category_name = kwargs.get("category_name", False)
        color = kwargs.get("color_names", False)
        brand_names = kwargs.get("brand_names", False)
        min_price = kwargs.get("min_price", False)
        max_price = kwargs.get("max_price", False)

        filter_flag = False

        if product_id:
            p_obj = Product.objects.filter(id=product_id)
            return p_obj

        if category_name and category_name not in ["None"]:
            q_obj = Q(category__category_name__icontains=category_name)
            q_object.add(q_obj, Q.AND)
            filter_flag = True

        if color and color not in ["None"]:
            q_obj = Q(pf_product__value__icontains=color) & Q(
                pf_product__parameter__icontains="color")
            q_object.add(q_obj, Q.AND)
            filter_flag = True

        if brand_names and brand_names not in ["None"]:
            q_obj = Q(brand__brand_name__icontains=brand_names)
            q_object.add(q_obj, Q.AND)
            filter_flag = True

        if min_price and min_price not in ["None"] and max_price and \
                max_price not in ["None"]:
            min_price, max_price = int(min_price), int(max_price)

            q_obj = Q(sale_price_as_float__range=(min_price, max_price))

            q_object.add(q_obj, Q.AND)
            filter_flag = True

        if filter_flag:
            p_obj = Product.objects.annotate(
                actual_price_as_float=Cast('actual_price', FloatField()),
                sale_price_as_float=Cast('sale_price', FloatField())).filter(q_object)
        else:
            p_obj = Product.objects.all()[:100]

        return p_obj

    def resolve_all_colors(self, info, **kwargs):
        """
        get color on given category_name, brand_names

        :param info:
        :param kwargs:
        :return:
        """
        q_object = Q()
        category_name = kwargs.get("category_name", False)
        brand_names = kwargs.get("brand_names", False)

        q_obj = Q(parameter__icontains="color")
        q_object.add(q_obj, Q.AND)

        if category_name and category_name not in ["None", None]:
            q_obj = Q(product__category__category_name__icontains=category_name)
            q_object.add(q_obj, Q.AND)

        if brand_names and brand_names not in ["None", None]:
            q_obj = Q(product__brand__brand_name__icontains=brand_names)
            q_object.add(q_obj, Q.AND)

        pf_obj = ProductFeatures.objects.filter(q_object)

        return pf_obj

    def resolve_all_brands(self, info, **kwargs):
        """
        get brand on the given category_name

        :param info:
        :param kwargs:
        :return:
        """
        q_object = Q()
        filter_flag = True

        category_name = kwargs.get("category_name", False)

        if category_name and category_name not in ["None"]:
            q_obj = Q(category__category_name__icontains=category_name)
            q_object.add(q_obj, Q.AND)
            filter_flag = True

        if filter_flag:
            brand_obj = Brand.objects.filter(q_object)

        else:
            brand_obj = Brand.objects.all()[:10]

        return brand_obj

    def resolve_all_categories(self, info, **kwargs):
        """
        get categories

        :param info:
        :param kwargs:
        :return:
        """
        cat_obj = Category.objects.all()[:100]
        return cat_obj

    def resolve_all_images(self, info, **kwargs):
        """
        get images

        :param info:
        :param kwargs:
        :return:
        """
        image_obj = ProductImages.objects.all()[:100]
        return image_obj
