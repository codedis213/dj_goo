QUERY_ALL_COLOR = """
{
  allColors(categoryName:"%(category_name)s" brandNames:"%(brand_name)s"){
    value
  }
}
"""

QUERY_ALL_BRANDS = """
{
  allBrands(categoryName:"%(category_name)s"){
    brandName
  }
}
"""

QUERY_ALL_PRODUCTS = """
query{
  allProducts(
  id:%(id)s,
  colorNames: "%(color_names)s", 
  categoryName: "%(category_names)s", 
  brandNames: "%(brand_names)s", 
  minPrice: "%(min_price)s",
  maxPrice: "%(max_price)s"
  ) {

    id
    name
    actualPrice
    salePrice
    actualPrice
    quantity
    descriptions
    pfProduct {
      parameter
      value
    }
    category {
      categoryName
      id
    }
    productImage {
      imageString
    }
    commentOnProduct {
      id
      userId {
        id
        username
      }
      commentText
      createdDate
      replySet {
        id
        replyText
        replyTo {
          id
          username
        }
        user {
          id
          username
        }
        CREATEDDate
      }
    }
    reviewOnProduct {
      commentText
      createdDate
      rating
      likedCount
      userId{
        id
        username
      }
      userLiked {
        id
        username
      }
      dislikeCount
      userDisliked {
        id
        username
      }
    }
  }
}

"""

if __name__ == "__main__":
    QUERY_ALL_PRODUCTS = QUERY_ALL_PRODUCTS % {"id": 112,
                                               "color_names": "Black",
                                               "category_names": "Ballerinas",
                                               "brand_names": "Bootwale",
                                               "min_price": "1795",
                                               "max_price": "4024"}
    print("#" * 20)
    print("get all products")
    print(QUERY_ALL_PRODUCTS)

    QUERY_ALL_BRANDS = QUERY_ALL_BRANDS % {"category_name": "Ballerinas"}
    print("#" * 20)
    print("get all brands")
    print(QUERY_ALL_BRANDS)
