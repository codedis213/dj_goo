from django.contrib import admin

from .models import Category, Brand, ProductAvailable, Product, CategorySpec, \
    ProductFeatures, ProductImages


class CategorySpecInline(admin.TabularInline):
    """
    Provide Inline Admin form  of Model CategorySpec
    """
    model = CategorySpec
    extra = 1


class CategoryAdmin(admin.ModelAdmin):
    """
    Provide Inline Admin form  of Model Category
    """
    search_fields = ['category_name']
    inlines = (CategorySpecInline,)


class BrandAdmin(admin.ModelAdmin):
    """"
    Provide Inline Admin form  of Model Brand
    """
    search_fields = ["get_brand_with_category"]


class ProductImagesInline(admin.TabularInline):
    """"
    Provide Inline Admin form  of Model ProductImages
    """
    model = ProductImages
    extra = 1


class ProductFeaturesInline(admin.TabularInline):
    """"
    Provide Inline Admin form  of Model ProductFeatures
    """
    model = ProductFeatures
    extra = 1


class ProductAvailableInline(admin.TabularInline):
    """"
    Provide Tabular Inline Admin form  of Model ProductAvailable
    """
    model = ProductAvailable
    extra = 1


class ProductAdmin(admin.ModelAdmin):
    """"
    Model Admin for  Product
    """
    autocomplete_fields = ['category', 'brand']
    search_fields = ["id", "name", "sku"]
    inlines = (
        ProductImagesInline, ProductFeaturesInline, ProductAvailableInline)


class ProductAvailableAdmin(admin.ModelAdmin):
    """"
   Model Admin for  ProductAvailable
   """

    def get_model_perms(self, request):
        """
        :param request:
        :return:
        """
        perms = admin.ModelAdmin.get_model_perms(self, request)
        perms['list_hide'] = True
        return perms


class CategorySpecAdmin(admin.ModelAdmin):
    """"
    Model Admin for  CategorySpec
    """

    def get_model_perms(self, request):
        """

        :param request:
        :return:
        """
        perms = admin.ModelAdmin.get_model_perms(self, request)
        perms['list_hide'] = True
        return perms


admin.site.register(Category, CategoryAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(ProductAvailable, ProductAvailableAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(CategorySpec, CategorySpecAdmin)
