# from django.contrib.postgres.fields import JSONField
from djongo import models
from account.models import CardDetail, UserSession
from common.models import Address


class CustomerSession(UserSession):
    """
    Model for Customer Session
    """
    customer = models.ForeignKey("account.User",
                                 related_name="customer_Session",
                                 on_delete=models.CASCADE)


class CustomerAddress(Address):
    """
    Model for Customer Address
    """
    customer = models.ForeignKey("account.User",
                                 related_name="customer_address",
                                 on_delete=models.CASCADE)
    flat_floor_no = models.CharField(max_length=30)
    is_billing_address = models.BooleanField(default=False)
    is_shipping_address = models.BooleanField(default=False)


class CustomerCardDetail(CardDetail):
    """
    Model for Customer Card Detail
    """
    customer = models.ForeignKey("account.User",
                                 related_name="customer_card_details",
                                 on_delete=models.CASCADE)
    is_primary = models.BooleanField(default=False)


class CustomerAddToBag(models.Model):
    """
    Model for Customer AddToBag
    """
    customer = models.ForeignKey("account.User", related_name="customer_bag",
                                 on_delete=models.CASCADE)
    product = models.ForeignKey("product.Product",
                                related_name="product_add_to_bag",
                                on_delete=models.CASCADE)


class CustomerWishList(models.Model):
    """
    Model for Customer WishList
    """
    customer = models.ForeignKey("account.User", related_name="customer_wish_list",
                                 on_delete=models.CASCADE)
    product = models.ForeignKey("product.Product",
                                related_name="product_wish_list",
                                on_delete=models.CASCADE)


class CustomerCompareList(models.Model):
    """
    Model for Customer Compare List
    """
    customer = models.ForeignKey("account.User",
                                 related_name="customer_compare_list",
                                 on_delete=models.CASCADE)
    product = models.ForeignKey("product.Product",
                                related_name="product_compare_list",
                                on_delete=models.CASCADE
                                )
