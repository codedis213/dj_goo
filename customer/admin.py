from django.contrib import admin
from .models import CustomerAddress, CustomerCardDetail, \
    CustomerSession, CustomerAddToBag, CustomerWishList, CustomerCompareList


class CustomerSessionInline(admin.TabularInline):
    """
    TabularInline Admin form for CustomerSession
    """
    model = CustomerSession
    extra = 1


class CustomerAddressInline(admin.TabularInline):
    """
    TabularInline Admin form for CustomerAddress
    """
    model = CustomerAddress
    extra = 1


class CustomerCardDetailInline(admin.TabularInline):
    """
    TabularInline Admin form for CustomerSession
    """
    model = CustomerCardDetail
    extra = 1


class CustomerAddToBagInline(admin.TabularInline):
    """
    TabularInline Admin form for CustomerAddToBag
    """
    model = CustomerAddToBag
    extra = 1


class CustomerWishListInline(admin.TabularInline):
    """
    TabularInline Admin form for CustomerWishList
    """
    model = CustomerWishList
    extra = 1


class CustomerCompareListInline(admin.TabularInline):
    """
    TabularInline Admin form for CustomerCompareList
    """
    model = CustomerCompareList
    extra = 1


class UserAdmin(admin.ModelAdmin):
    """
    ModelAdmin for Customer
    """
    inlines = (
        CustomerAddressInline, CustomerCardDetailInline)


admin.site.register(CustomerAddress)
admin.site.register(CustomerCardDetail)
admin.site.register(CustomerSession)
admin.site.register(CustomerAddToBag)
admin.site.register(CustomerWishList)
admin.site.register(CustomerCompareList)
