import graphene
from graphene_django import DjangoObjectType
from account.models import User


class CustomerType(DjangoObjectType):
    class Meta:
        model = User


class Query(graphene.ObjectType):
    customers = graphene.List(CustomerType)

    def resolve_customers(self, info, **Kwargs):
        return User.objects.filter(is_customer=True)[:10]


class CreateCustomer(graphene.Mutation):
    id = graphene.Int()
    username = graphene.String()

    class Arguments:
        username = graphene.String()
        first_name = graphene.String()
        last_name = graphene.String()
        email = graphene.String()
        password = graphene.String()

    def mutate(self, info, username, first_name, last_name, email, password):
        user = User.objects.create_user(username=username, first_name=first_name,
                                        last_name=last_name, password=password, email=email)
        user.is_customer = True
        user.save()
        return CreateCustomer(
            id=User.id,
            username=User.username
        )


class Mutation(graphene.ObjectType):
    create_customer = CreateCustomer.Field()
