from djongo import models

from account.models import User
from donnyji.donnyji_constants import TYPE_OF_PAYMENT_CHOICE


class Payment(models.Model):
    """
    Model Payment
    """
    method = models.CharField(max_length=15, choices=TYPE_OF_PAYMENT_CHOICE)
    payment_doer = models.ForeignKey(
        User, related_name="payment_doer",
        on_delete=models.CASCADE)
    payment_receiver = models.ForeignKey(
        User,
        related_name="payment_receiver",
        on_delete=models.CASCADE)
    payment_amount = models.CharField(max_length=15)
    is_payment_by_cash = models.BooleanField()
    is_payment_by_card = models.BooleanField()
    is_payment_delivered = models.BooleanField()
    is_payment_received = models.BooleanField()
    payment_initiated_datetime = models.DateTimeField(auto_now_add=True)
    payment_completed_date_time = models.DateTimeField()
