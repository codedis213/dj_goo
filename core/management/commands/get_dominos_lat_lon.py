import csv
import os
import sys

import django
import requests
from bs4 import BeautifulSoup

# set up the Django enviroment
sys.path.append("/home/jai/dj_mongo/donnyji/core/management/commands")
sys.path.append("/home/jai/dj_mongo/donnyji/")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'donnyji.settings')
django.setup()
from store.models import StoreHouse


class DomimnosLatLon:
    """
    parse the Dominos Website for the Dominos addresses
    https://www.dominos.co.in/store-location/
    """

    def __init__(self, output_csv_file_path):
        """
        parse the Dominos Website for the Dominos addresses
        https://www.dominos.co.in/store-location/
        """
        self.dominos_addresses = []
        self.output_csv_file_path = output_csv_file_path
        self.url_list = []

    def extract_all_location_urls(self, url):
        """
        given a url, extract all the urls
        :param url:
        :return:
        """

        res = requests.get(url)
        html = res.content
        soup = BeautifulSoup(html, "html.parser")
        ul = soup.find("ul", attrs={"class": "citylist-ul"})
        a_list = ul.find_all("a", attrs={"class": "citylink"})

        for a in a_list:
            link = "https://www.dominos.co.in{}".format(a.get("href"))
            self.url_list.append(link)

    def get_dominos_addresses(self, url):
        """
        get dominos address from the given list of urls

        :param url:
        :return:
        """
        response = requests.get(url)
        html = response.content

        soup = BeautifulSoup(html, "html.parser")
        store_div_list = soup.find_all("div", attrs={
            "class": "panel panel-default custom-panel"})

        for store_div in store_div_list:
            p = store_div.find("p", attrs={"class": "grey-text mb-0"})
            address = p.get_text()
            self.dominos_addresses.append(address)

    def addresses_into_csv(self, dominos_addresses, output_csv_file_path):
        """
        save the given list of dominos addresses into the csv
        :param dominos_addresses:
        :param output_csv_file_path:
        :return:
        """

        with open(output_csv_file_path, "w+", newline='') as fp:
            wr = csv.writer(fp, dialect='excel')
            wr.writerow(["Dominos Addresses"])
            for val in dominos_addresses:
                wr.writerow([val])

    def parse_url_for_address(self, url_list):
        """
        parse the given list of urls for the dominos addresses
        and save the result into the csv

        :param url_list:
        :return:
        """
        for url in url_list:
            self.get_dominos_addresses(url)

        self.addresses_into_csv(
            dominos_addresses=self.dominos_addresses,
            output_csv_file_path=self.output_csv_file_path)

        print(self.dominos_addresses)
        print(self.dominos_addresses.__len__())

    def get_lat_lon_of_address(self, address):
        """
        get lat and lon of the given address
        :param address:
        :return:
        """
        lat, lng = 0, 0

        try:
            params = {"location": address,
                      "key": "qmd6hp3zOBxLa8DsPlYqQXM2mckldAXI"}
            res = requests.get(
                "http://www.mapquestapi.com/geocoding/v1/address",
                params=params)
            res.close()
            lat_lon = res.json()["results"][0]["locations"][0]["latLng"]
            lat = lat_lon["lat"]
            lng = lat_lon["lng"]

        except Exception as error:
            print("error: {}".format(error))

        return lat, lng

    def google_get_lat_lon_of_address(self, address):
        """
        get lat and lon of the given address
        :param address:
        :return:
        """
        lat, lng = 0, 0

        try:
            params = {"address": address,
                      "key": "AIzaSyCqLGNfBd1TTJLBSDcDjeC0yufPnEjmybs"}
            res = requests.get(
                "https://maps.googleapis.com/maps/api/geocode/json",
                params=params)
            res.close()
            lat = res.json()["results"][0]["geometry"]["location"]["lat"]
            lng = res.json()["results"][0]["geometry"]["location"]["lng"]

        except Exception as error:
            print("error: {}".format(error))

        return lat, lng

    def parse_csv_fill_address_lat_lon(self, csv_file):
        """
        parse the address from the csv and hit the api and get lat lon
        and save all into the dominos_address_lat_lon.csv
        :param csv_file:
        :return:
        """

        with open('dominos_address_lat_lon.csv', mode='w') as employee_file:
            employee_writer = csv.writer(employee_file, delimiter=',',
                                         quotechar='"',
                                         quoting=csv.QUOTE_MINIMAL)

            with open(csv_file) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    if line_count == 0:
                        print(", ".join(row))
                        line_count += 1
                    else:
                        address = row[0]
                        lat, lon = self.get_lat_lon_of_address(address=address)
                        if lat == 0 and lon == 0:
                            pass
                        else:
                            employee_writer.writerow([address, lat, lon])
                            print((address, lat, lon))
                        line_count += 1

    def google_parse_csv_fill_address_lat_lon(self, csv_file):
        """
        parse the address from the csv and hit the api and get lat lon
        and save all into the dominos_address_lat_lon.csv
        :param csv_file:
        :return:
        """

        with open('dominos_address_lat_lon.csv', mode='w') as employee_file:
            employee_writer = csv.writer(employee_file, delimiter=',',
                                         quotechar='"',
                                         quoting=csv.QUOTE_MINIMAL)

            with open(csv_file) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    if line_count == 0:
                        print(", ".join(row))
                        line_count += 1
                    else:
                        address = row[0]
                        lat, lon = self.google_get_lat_lon_of_address(
                            address=address)
                        if lat == 0 and lon == 0:
                            pass
                        else:
                            employee_writer.writerow([address, lat, lon])
                            print((address, lat, lon))
                        line_count += 1

    def fill_seller_lat_lon(self, csv_name):
        """
        get the csv file name having the dominos address, and its lat and lon
        read the line one by one
        map each seller storehouse address, lat and lon with the unique
        dominos address, lat and lon
        :param csv_name:
        :return:
        """

        store_house_obj_lst = StoreHouse.objects.all()
        store_house_obj_len = len(store_house_obj_lst)
        row_index = 0
        loop = True

        while loop:
            with open(csv_name) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')

                for row in csv_reader:
                    address, lat, lon = row[0], row[1], row[2]

                    if row_index < store_house_obj_len:
                        store_house_obj_lst[row_index].search_address = address
                        store_house_obj_lst[row_index].lat = lat
                        store_house_obj_lst[row_index].lon = lon
                        try:
                            store_house_obj_lst[row_index].save()
                        except Exception as error:
                            print("error: {}".format(error))

                        print(store_house_obj_lst[row_index].id)
                        row_index += 1

                if row_index >= store_house_obj_len:
                    loop = False


def sub_main(output_csv_file_path):
    """
    create an object of DomimnosLatLon and
    get all the addresses of Dominos stores on the the given
    :return:
    """
    obj = DomimnosLatLon(output_csv_file_path)

    url = "https://www.dominos.co.in/store-location/"
    # obj.extract_all_location_urls(url=url)
    # url_list = obj.url_list
    # print(url_list)
    # obj.parse_url_for_address(url_list=url_list)

    # obj.parse_csv_fill_address_lat_lon(output_csv_file_path)
    obj.google_parse_csv_fill_address_lat_lon(output_csv_file_path)

    obj.fill_seller_lat_lon("dominos_address_lat_lon.csv")


if __name__ == "__main__":
    output_csv_file_path = "dominos_addresses.csv"
    sub_main(output_csv_file_path)
