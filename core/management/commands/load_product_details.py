import ast
import csv
import datetime
import json
import uuid

from django.core.management.base import BaseCommand

from account.models import User
from product.models import Category, Product, ProductImages, ProductFeatures, \
    CategorySpec, Brand
from seller.models import Seller
from store.models import StoreHouse


class Command(BaseCommand):
    """
    Command to load the csv  into db
    """
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        """
        method to argument into parser
        :param parser:
        :return:
        """
        parser.add_argument('load_from', nargs='+', type=str)

    def load_from_csv_file(self):

        """
        uniq_id,crawl_timestamp,product_url,product_name,product_category_tree,
        pid,retail_price,discounted_price,image,is_FK_Advantage_product,
        description,product_rating,overall_rating,brand,product_specifications
        :return:
        """

        csv_file = open(
            'core/management/commands/flipkart_com-ecommerce_sample.csv')
        csv_reader = csv.DictReader(csv_file)

        for pde_obj in csv_reader:
            try:
                use_obj, _ = User.objects.get_or_create(
                    username=str(pde_obj["brand"]).strip())
                use_obj.set_password("abc@123456")
                seller_obj, _ = Seller.objects.get_or_create(
                    seller=use_obj,
                    id_proof_no="{}_{}".format(
                        use_obj.pk, use_obj.username))
                stor_obj, _ = StoreHouse.objects.get_or_create(
                    seller=seller_obj)

                product_category_tree = ast.literal_eval(
                    pde_obj["product_category_tree"])

                product_category_tree = product_category_tree[0].split(">>")[
                                        :3]
                index = 1
                parent_category = sub_category = s_s_category = None
                for cat in product_category_tree:
                    if index == 1:
                        parent_category, _ = Category.objects.get_or_create(
                            category_name=cat)
                    elif index == 2:
                        sub_category, _ = Category.objects.get_or_create(
                            category_name=cat,
                            parent_category=parent_category)
                    elif index == 3:
                        s_s_category, _ = Category.objects.get_or_create(
                            category_name=cat,
                            parent_category=sub_category)
                    index += 1
                cat_obj = s_s_category or sub_category or parent_category

                b_obj, _ = Brand.objects.get_or_create(
                    category=cat_obj,
                    brand_name=pde_obj["brand"])

                product_dict = {
                    "store_house": stor_obj,
                    "category": s_s_category or sub_category or parent_category,
                    "name": str(pde_obj["product_name"]).strip(),
                    "brand": b_obj,
                    "descriptions": pde_obj["description"],
                    "actual_price": pde_obj["retail_price"],
                    "sale_price": pde_obj["discounted_price"],
                    "quantity": 5,
                    "item_sold": 0,
                    "returned_item": 0,
                    "product_rating": pde_obj["product_rating"]
                }

                pro_obj, _ = Product.objects.get_or_create(
                    **product_dict)

                pro_obj.sku = uuid.uuid1()
                pro_obj.manufacture_date = datetime.datetime.now()
                pro_obj.expire_date = datetime.datetime.now()
                pro_obj.save()

                product_images_list = ast.literal_eval(pde_obj["image"])

                for image_dict in product_images_list:
                    ProductImages.objects.get_or_create(
                        product=pro_obj,
                        image_string=image_dict)

                product_description = ast.literal_eval(
                    pde_obj["product_specifications"].replace("=>", ":"))[
                    "product_specification"]

                for pro_desc in product_description:
                    if not (pro_desc.get("key") and pro_desc.get("value")):
                        continue

                    ProductFeatures.objects.get_or_create(
                        product=pro_obj,
                        parameter=str(pro_desc["key"]).strip(),
                        value=str(pro_desc["value"]).strip())

                    CategorySpec.objects.get_or_create(
                        category=parent_category,
                        params_name=str(pro_desc["key"]).strip())
                    CategorySpec.objects.get_or_create(
                        category=sub_category,
                        params_name=str(pro_desc["key"]).strip())
                    CategorySpec.objects.get_or_create(
                        category=s_s_category,
                        params_name=str(pro_desc["key"]).strip())

                self.stdout.write(
                    self.style.SUCCESS('Successfully added "%s"' %
                                       pde_obj["product_name"]))
            except Exception as error:
                self.stdout.write(
                    self.style.SUCCESS(
                        'Failed to add "{}" error: {}'.format(
                            pde_obj["product_name"], error)
                    ))

    def load_seller_n_store(self):
        """
        load seller and its lat and lon information into the
        seller and seller address table respectively
        :return:
        """

        f = open(
            "core/management/commands/SellerAddressLatLon.json")
        f_read = f.read()
        f.close()
        slr_obj_list = json.loads(f_read)

        for slr_obj in slr_obj_list:
            slr_obj = slr_obj["fields"]
            try:
                if not slr_obj["full_address"]:
                    continue

                use_obj, created = User.objects.get_or_create(
                    username=str(slr_obj["seller_name"]).strip())
                use_obj.set_password("abc@123456")

                seller_obj, created = Seller.objects.get_or_create(
                    seller=use_obj,
                    id_proof_no="{}_{}".format(
                        use_obj.pk, use_obj.username))

                stor_obj, created = StoreHouse.objects.get_or_create(
                    seller=seller_obj, lat=slr_obj["latitude"],
                    lon=slr_obj["longitude"])

                if slr_obj["full_address"]:
                    stor_obj.search_address = slr_obj["full_address"]
                    stor_obj.save()

                self.stdout.write(
                    self.style.SUCCESS('Successfully added "%s"' %
                                       slr_obj["seller_name"]))
            except Exception as error:
                self.stdout.write(
                    self.style.SUCCESS(
                        'Failed to add "{}" error: {}'.format(
                            slr_obj["seller_name"], error)
                    ))

    def load_seller_into_product(self):
        """
        load seller into the product table
        :return:
        """
        f = open(
            "core/management/commands/SellerAddressLatLon.json")
        f_read = f.read()
        f.close()
        slr_obj_list = json.loads(f_read)

        seller_name_list = [o["fields"]["seller_name"] for o in
                            slr_obj_list if
                            o["fields"]["full_address"]]

        obj = Product.objects.all().values(
            "store_house__seller__seller__username")

        product_seller_name = list(set([
            o["store_house__seller__seller__username"]
            for o in obj]))

        length = product_seller_name.__len__()
        seller_length = seller_name_list.__len__()
        index = 0
        inner_index = 0
        seller_name_mapping = dict()

        while index < length:
            if inner_index == seller_length:
                inner_index = 0
            seller_name_mapping[product_seller_name[index]] = \
                seller_name_list[
                    inner_index]

            inner_index += 1
            index += 1

        pro_obj_list = Product.objects.all()

        for pro_obj in pro_obj_list:

            try:
                if not pro_obj.store_house:
                    sh_obj = StoreHouse.objects.filter(
                        seller__seller__username=seller_name_list[0])[0]
                else:
                    seller_name = pro_obj.store_house.seller.seller.username
                    seller_name = seller_name_mapping[seller_name]
                    sh_obj = StoreHouse.objects.filter(
                        seller__seller__username=seller_name)[0]
                pro_obj.store_house = sh_obj
                pro_obj.save()

                self.stdout.write(
                    self.style.SUCCESS('Successfully added "%s"' %
                                       pro_obj.store_house.seller.seller.username))
            except Exception as error:
                self.stdout.write(
                    self.style.SUCCESS(
                        'Failed to add "{}" error: {}'.format(
                            pro_obj.name, error)
                    ))

    def handle(self, *args, **options):
        """
        handle the arguments and options, given from terminal
        :param args:
        :param options:
        :return:
        """
        load_from = options.get("load_from")

        if load_from == ['load_from', 'csv_file']:
            self.load_from_csv_file()
            self.load_seller_n_store()
            self.load_seller_into_product()
