class Utilities:
    """
    Donyyji Utilities
    """

    def __init__(self):
        pass

    def __make_category_hierarchy(self, category_obj_list,
                                  category_dict: dict = None):
        """

        :param category_obj_list:
        :param category_dict:
        :return:
        """

        for category_obj in category_obj_list:
            sub_category_list = category_obj.cat_parent_category.all()
            if sub_category_list:
                self.__make_category_hierarchy(
                    category_obj_list=sub_category_list,
                    category_dict=category_dict)
                category_dict[category_obj.category_name] = [
                    sub_category.category_name for sub_category in
                    sub_category_list]

        return category_dict

    def make_category_hierarchy(self, category_obj_list):
        """

        :param category_obj_list:
        :return:
        """
        category_dict = {}
        category_name_list = [category.category_name for category in
                              category_obj_list]
        category_dict = self.__make_category_hierarchy(category_obj_list,
                                                       category_dict,
                                                       )

        final_category_dict = {}

        for category_name in category_name_list:
            sub_category_list = category_dict.get(category_name, [])
            length = sub_category_list.__len__()
            index = 0
            while index < length:
                sub_category = sub_category_list[index]
                ss_cat_list = category_dict.get(sub_category, [])
                sub_category_list[index] = {sub_category: ss_cat_list}
                index += 1
            final_category_dict[category_name] = sub_category_list
        return final_category_dict
