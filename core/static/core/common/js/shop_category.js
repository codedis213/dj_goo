$(document).ready(function() {

    var side_bar_options = {}

    /* on select category on the shop product page */

    $(".select_category").click(function() {
        category_name = $(this).attr('data-cat');

        console.debug("before clear side_bar dict" + side_bar_options)

        for (var prop in side_bar_options) {
            if (side_bar_options.hasOwnProperty(prop)) {
                delete side_bar_options[prop];
            }
        }
        console.debug("after clear side_bar dict" + side_bar_options)

        sendInfo = {
            category_name: category_name
        }

        side_bar_options["category_name"] = category_name

        var ajax1 = $.ajax({
            url: "/get_brands_n_colors/",
            async: true,
            data: sendInfo,
            success: function(result) {}
        });

        var ajax2 = $.ajax({
            url: "/get_products/",
            async: true,
            data: sendInfo,
            success: function(result) {}
        });

        $.when(ajax1, ajax2).done(function(a1, a2) {
            // a1 and a2 are arguments resolved for the page1 and page2 ajax requests, respectively.
            // Each argument is an array with the following structure: [ data, statusText, jqXHR ]
            var data = a1[0] + a2[0]; // a1[ 0 ] = "Whip", a2[ 0 ] = " It"
            $('#brand_side_bar').html(a1[0]);
            $('#product_container').html(a2[0]);
        });

    });

    /* on select brand on the shop product page  */

    $("#brand_side_bar").on('click', "#brand_widgets_inner input[type='checkbox']", function() {

        var checkBoxes = $(this);

        checkBoxes.attr("checked", !checkBoxes.attr("checked"));
        //        checkBoxes.prop("checked", !checkBoxes.prop("checked"));

        var favorite = [];

        $.each($("#brand_widgets_inner input[type='checkbox']:checked"), function() {
            favorite.push($(this).attr('value'));
        });

        side_bar_options["selected_brands"] = favorite
        console.debug("My favourite sports are: " +
            side_bar_options["category_name"],
            side_bar_options["selected_brands"])

        var ajax2 = $.ajax({
            url: "/get_products/",
            async: true,
            data: side_bar_options,
            success: function(result) {
                $('#product_container').html(result);
                console.debug(result)
            }
        });

    });

    /* on select color on the shop product page */

    $("#brand_side_bar").on('click', "#color_widgets_inner input[type='checkbox']", function() {

        var checkBoxes = $(this);

        checkBoxes.attr("checked", !checkBoxes.attr("checked"));
        //        checkBoxes.prop("checked", !checkBoxes.prop("checked"));

        var favorite = [];

        $.each($("#color_widgets_inner input[type='checkbox']:checked"), function() {
            favorite.push($(this).attr('value'));
        });

        side_bar_options["selected_color"] = favorite
        console.debug("My favourite sports are: " +
            side_bar_options["category_name"] +
            side_bar_options["selected_brands"] +
            side_bar_options["selected_color"])

        var ajax2 = $.ajax({
            url: "/get_products/",
            async: true,
            data: side_bar_options,
            success: function(result) {
                $('#product_container').html(result);
                console.debug(result)
            }
        });

    });

    /* select on price slider */

    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 15000,
        values: [75, 3000],
        slide: function(event, ui) {
            $("#amount").val("₹" + ui.values[0] + " - ₹" + ui.values[1]);

            side_bar_options["selected_min_price"] = ui.values[0]
            side_bar_options["selected_max_price"] = ui.values[1]

            var ajax3 = $.ajax({
                url: "/get_products/",
                async: true,
                data: side_bar_options,
                success: function(result) {
                    $('#product_container').html(result);
                    console.debug(result)
                }
            });

        }
    });

    /* select on the slider amount */

    $("#amount").val("₹" + $("#slider-range").slider("values", 0) +
        " - ₹" + $("#slider-range").slider("values", 1));

    /* send on click of category image redirect to product details */

    $('body').on('click', '#single_category_img img', function() {
        var img_tag = $(this);
        console.log(img_tag.attr("prodid"))
        window.location.href = "/product_detail/" + img_tag.attr("prodid");

    });

    /* send on click of category product redirect to product details */

    $('body').on('click', '#category_product_text', function() {
        var img_tag = $(this);
        console.log(img_tag.attr("prodid"))
        window.location.href = "/product_detail/" + img_tag.attr("prodid");

    });

    /* function to make form serialize array into dict */

    function makeDict(data) {
        var values = {}
        $.each(data, function(i, field) {
            values[field.name] = field.value;
        });
        return values

    }

    /* function to send review content to server */

    function createReview(data) {
        data_dict = makeDict(data)
        rating = data_dict["rating"]
        message = data_dict["message"]

        if (!rating) {
            alert("Please select rating also to review");
            return
        } else if (!message) {
            alert("Please enter the review message to review");
            return
        }

        data = $.param(data);

        $.ajax({
            url: "/post_review/",
            async: true,
            data: data,
            type: 'POST',
            success: function(result) {
                console.debug(result)
                $('#review_list').prepend(result);
            }
        });
    }

    /* send review form content to server */

    $('body').on('click', '#button_add_review', function() {
        event.preventDefault();
        createReview($('#form_add_review').serializeArray());

    });

    /* set rating */
    function responseMessage(msg) {
        $('#rating').val(msg);
    }

    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#stars li').on('mouseover', function() {
        var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

        // Now highlight all the stars that's not after the current hovered star
        $(this).parent().children('li.star').each(function(e) {
            if (e < onStar) {
                $(this).addClass('hover');
            } else {
                $(this).removeClass('hover');
            }
        });

    }).on('mouseout', function() {
        $(this).parent().children('li.star').each(function(e) {
            $(this).removeClass('hover');
        });
    });

    /* 2. Action to perform on click */
    $('#stars li').on('click', function() {
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('li.star');

        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('selected');
        }

        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('selected');
        }

        /* rating selection here */

        var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
        responseMessage(ratingValue);

    });

    /* function to post comment */

    function createComment(data) {
        data_dict = makeDict(data)
        message = data_dict["message"]

        if (!message) {
            alert("Please put some comment");
            return
        }

        data = $.param(data);

        $.ajax({
            url: "/post_comment/",
            async: true,
            data: data,
            type: 'POST',
            success: function(result) {
                console.debug(result)
                $('#comment_list').prepend(result);
            }
        });
    }

    /* send serialized comment form data to server */

    $('body').on('click', '#button_add_comment', function() {
        event.preventDefault();
        createComment($('#form_add_comment').serializeArray());
    });

    /* show model on click of the reply_btn and fill the reply form */
    $('body').on('click', '.reply_btn', function() {
        var parent_comment_id = $(this).attr('parent-comment-id')
        var reply_from = $(this).attr('reply-from')
        var reply_to = $(this).attr('reply-to')
        var productId = $(this).attr('productId')
        $('#productId').val(productId);
        $('#parentCommentId').val(parent_comment_id);
        $('#replyFrom').val(reply_from);
        $('#replyTo').val(reply_to);
        $('#replyModal').modal('show');
    });

    /* function to post reply */

    function postReply(data) {
        data_dict = makeDict(data)

        parentCommentId = data_dict["parentCommentId"]
        message = data_dict["replyText"]

        if (!message) {
            alert("Please put some reply message");
            return
        }
        else{
            $('#replyModal').modal('hide');
        }

        data = $.param(data);
        console.log(data)

        $.ajax({
            url: "/post_reply/",
            async: true,
            data: data,
            type: 'POST',
            success: function(result) {
                console.debug(result)
                $('#reply-item-'+ parentCommentId).prepend(result);
            }
        });
    }


     /* post reply data to server and hide reply model */
     $('body').on('click', '#sendReply', function() {
        event.preventDefault();
        postReply($('#replyForm').serializeArray());
    });


});