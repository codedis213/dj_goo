from rest_framework.viewsets import ViewSet

from core.schema import schema
from core.utilities import Utilities
from product.gpl_query_constant import QUERY_ALL_COLOR, QUERY_ALL_BRANDS, \
    QUERY_ALL_PRODUCTS
from product.models import Category


class ProductListingsHelper(ViewSet):
    """
    Helper Class containing Methods To show the List of Product
    """

    @staticmethod
    def extract_filters(request, format=None) -> dict:
        """
        extract and return filters like category name, brand name and color
        from the request object
        :param request:
        :param format:
        :return:
        """
        filters_dict = dict()

        filters_dict["category_name"] = request.GET.get(
            "category_name") or "School Bags"

        brand_names = request.GET.getlist('selected_brands[]')

        if brand_names:
            filters_dict["brand_names"] = brand_names[0]

        color_names = request.GET.getlist('selected_color[]')

        if color_names:
            filters_dict["color_names"] = color_names[0]

        filters_dict["min_price"] = request.GET.get('selected_min_price')
        filters_dict["max_price"] = request.GET.get('selected_max_price')

        return filters_dict

    @staticmethod
    def get_categories_gql(request, format=None):
        """
        return hierarchy of categories
        :param request:
        :param format:
        :return:
        """
        data = Category.objects.filter(
            parent_category__isnull=True).select_related('parent_category')

        ut_obj = Utilities()
        return ut_obj.make_category_hierarchy(data)

    @staticmethod
    def get_brands_gql(category_name: str = "School Bags"):
        """
        return list of brands on the given category_name

        :param category_name:
        :return:
        """
        brands_query_string = QUERY_ALL_BRANDS % {
            "category_name": category_name
        }

        brands_objs = schema.execute(brands_query_string)

        brand_dict = dict()

        for obj in brands_objs.data["allBrands"]:
            if not brand_dict.get(obj.get("brandName")):
                brand_dict[obj.get("brandName")] = obj.get("brandName")

        return brand_dict

    @staticmethod
    def get_colors_gql(category_name: str = "School Bags",
                       brand_name: str = None):
        """
        return colors on the given category name and brand name

        :param category_name:
        :param brand_name:
        :return:
        """
        color_query_string = QUERY_ALL_COLOR % {
            "category_name": category_name,
            "brand_name": brand_name}

        color_obj = schema.execute(color_query_string)

        color_dict = dict()

        for obj in color_obj.data["allColors"]:
            if not color_dict.get(obj.get("value")):
                color_dict[obj.get("value")] = obj.get("value")

        return color_dict

    @staticmethod
    def get_products_gql(*args, **kwargs):
        """
        return list of products on the category name, brand names,
        color name
        :param args:
        :param kwargs:
        :return:
        """

        product_id = kwargs.get("id", 0)
        category_name = kwargs.get("category_name", "School Bags")
        brand_names = kwargs.get("brand_names")
        color_names = kwargs.get("color_names")
        min_price = kwargs.get("min_price")
        max_price = kwargs.get("max_price")

        query_all_products_string = QUERY_ALL_PRODUCTS % {
            "id": product_id,
            "color_names": color_names,
            "category_names": category_name,
            "brand_names": brand_names,
            "min_price": min_price,
            "max_price": max_price
        }

        all_products_objs = schema.execute(query_all_products_string)
        return all_products_objs.data["allProducts"]
