from django.http import JsonResponse
from django.shortcuts import render
from account.models import User
from django.contrib import messages
from django.contrib.auth import login as user_login, logout as user_logout
from core.helpers import ProductListingsHelper
from django.contrib.auth.models import auth


def login(request):
    """
    login method
    :param request:
    :return:
    """
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return render(request, 'core/listed/listed/index.html')
        else:
            messages.info(request, 'invalid username or password')
    return render(request, 'core/winter/login.html')


def logout(request):
    """
    logout the user
    :param request:
    :return:
    """
    user_logout(request)
    return render(request, 'core/listed/listed/index.html')


def signup(request):
    """
    signup to user
    :param request:
    :return:
    """
    if request.method == 'POST':
        username = request.POST.get('username')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        password = request.POST.get('password')
        if User.objects.filter(username=username).exists():
            messages.error(request, 'username already exists')
            return render(request, 'core/winter/login.html')
        elif User.objects.filter(email=email).exists():
            messages.error(request, 'username already exists')
            return render(request, 'core/winter/login.html')
        else:
            user = User.objects.create_user(username=username, first_name=first_name,
                                            last_name=last_name, password=password, email=email)
            user.is_customer = True
            user.save()
            user_login(request, user)
            return render(request, 'core/listed/listed/index.html')
    return render(request, 'core/winter/login.html')


def index(request):
    """

    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/listed/listed/index.html', context)


class ProductListings(ProductListingsHelper):
    """
    retrieve records for the product list page
    """

    def get_categories(self, request, format=None):
        """
        return hierarchy of categories
        :param request:
        :param format:
        :return:
        """
        categories = self.get_categories_gql(request=request,
                                             format=format)
        return JsonResponse(categories)

    def get_brands_n_colors(self, request, format=None):
        """
        return brands and colors on the given category
        :param request:
        :param format:
        :return:l
        """

        filters_dict = self.extract_filters(request=request)

        brands_obj = self.get_brands_gql(
            category_name=filters_dict.get("category_name"))
        colors_obj = self.get_colors_gql(
            category_name=filters_dict.get("category_name"))

        context = {'brands_obj': brands_obj,
                   "colors_obj": colors_obj}

        return render(request, 'core/winter/brand_side_bar.html', context)

    def product_listings(self, request, format=None):
        """
        return list of products, on given category name, brand name and
        color name etc
        :param request:
        :param format:
        :return:
        """

        categories = self.get_categories_gql(request=request,
                                             format=format)

        filters_dict = self.extract_filters(request=request)

        products_objs = self.get_products_gql(
            category_name=filters_dict.get("category_name"),
            brand_names=filters_dict.get("brand_names"),
            color_names=filters_dict.get("color_names"),
            min_price=filters_dict.get("min_price"),
            max_price=filters_dict.get("max_price"),
        )

        brands_obj = self.get_brands_gql(
            category_name=filters_dict.get("category_name"))
        colors_obj = self.get_colors_gql(
            category_name=filters_dict.get("category_name"))

        context = {'browse_categories': categories,
                   'brands_obj': brands_obj,
                   "browse_products": products_objs,
                   "colors_obj": colors_obj,
                   "category_name": filters_dict.get("category_name")}

        return render(request, 'core/winter/category.html', context)

    def get_products(self, request, format=None):
        """
        return products on the given brand, category and other filters
        :param request:
        :param format:
        :return:
        """

        filters_dict = self.extract_filters(request=request)

        products_objs = self.get_products_gql(
            category_name=filters_dict.get("category_name"),
            brand_names=filters_dict.get("brand_names"),
            color_names=filters_dict.get("color_names"),
            min_price=filters_dict.get("min_price"),
            max_price=filters_dict.get("max_price"),
        )

        context = {
            "browse_products": products_objs,
            "category_name": filters_dict.get("category_name")}

        return render(request, 'core/winter/products_listing.html', context)

    def product_detail(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        product_id = kwargs.get("pk")

        products_objs = self.get_products_gql(id=product_id)

        context = {
            "browse_products": products_objs[0]
        }

        return render(request, 'core/winter/single-product.html', context)


def about(request):
    """

    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/winter/about.html', context)


def contact(request):
    """

    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/winter/contact.html', context)


def listings(request):
    """

    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/listed/listed/listings.html', context)


def category(request):
    """

    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/winter/category.html', context)


def listings_single(request):
    """

    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/listed/listed/listings-single.html', context)


def tracking(request):
    """

    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/winter/tracking.html', context)


def checkout(request):
    """

    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/winter/checkout.html', context)


def cart(request):
    """

    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/winter/cart.html', context)


def confirmation(request):
    """

    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/winter/confirmation.html', context)


def elements(request):
    """

    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/winter/elements.html', context)


def blog(request):
    """

    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/winter/blog.html', context)


def single_blog(request):
    """
    
    :param request:
    :return:
    """
    context = {'latest_question_list': "latest_question_list"}
    return render(request, 'core/winter/single-blog.html', context)
