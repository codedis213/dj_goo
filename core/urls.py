from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from rest_framework.urlpatterns import format_suffix_patterns

from communication.views import ProductReviewComments
from core import views
from core.views import ProductListings

PRODUCT_LISTINGS = ProductListings.as_view(
    actions={'get': 'product_listings'})
GET_CATEGORIES = ProductListings.as_view(
    actions={'get': 'get_categories'})
GET_BRADS_N_COLORS = ProductListings.as_view(
    actions={'get': 'get_brands_n_colors'})
GET_PRODUCTS = ProductListings.as_view(
    actions={'get': 'get_products'})
PRODUCT_DETAIL = ProductListings.as_view(
    actions={'get': 'product_detail'})

app_name = "core"

# urlpatterns for web response
urlpatterns = [
    url(r'^product_listings', PRODUCT_LISTINGS, name='product_listings'),
    url(r'^get_products', GET_PRODUCTS, name='get_products'),
    url(r'^get_categories', GET_CATEGORIES, name='get_categories'),
    url(r'^get_brands_n_colors', GET_BRADS_N_COLORS,
        name='get_brands_n_colors'),
    url(r'^single_product/(?P<pk>\d+)/$', PRODUCT_DETAIL,
        name='single_product'),
    url(r'^product_detail/(?P<pk>\d+)/$', PRODUCT_DETAIL,
        name='product_detail'),
]
# urlpatterns for review comments

POST_REVIEW_ACTION = ProductReviewComments.as_view(
    actions={'post': 'post_review'})
POST_COMMENT_ACTION = ProductReviewComments.as_view(
    actions={'post': 'post_comment'})
POST_REPLY_ACTION = ProductReviewComments.as_view(
    actions={'post': 'post_reply'})

urlpatterns += [
    url(r'^post_review', csrf_exempt(POST_REVIEW_ACTION), name='post_review'),
    url(r'^post_comment', csrf_exempt(POST_COMMENT_ACTION), name='post_comment'),
    url(r'^post_reply', csrf_exempt(POST_REPLY_ACTION), name='post_reply')

]

urlpatterns += [
    url(r'^$', views.index, name='index'),
    url(r'^login$', views.login, name='login'),
    url(r'^logout$', views.logout, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^about$', views.about, name='about'),
    url(r'^blog$', views.blog, name='blog'),
    url(r'^contact$', views.contact, name='contact'),
    url(r'^listings$', views.listings, name='listings'),
    url(r'^listings_single$', views.listings_single, name='listings_single'),
    url(r'^tracking$', views.tracking, name='tracking'),
    url(r'^checkout$', views.checkout, name='checkout'),
    url(r'^cart$', views.cart, name='cart'),
    url(r'^confirmation$', views.confirmation, name='confirmation'),
    url(r'^elements$', views.elements, name='elements'),
    url(r'^single_blog$', views.single_blog, name='single_blog'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
