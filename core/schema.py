import graphene

import common.schema
import communication.schema
import product.schema
import customer.schema


class Query(product.schema.Query,
            communication.schema.Query,
            common.schema.Query,
            customer.schema.Query,
            graphene.ObjectType):
    """
    # This class will inherit from multiple Queries
    # as we begin to add more apps to our project
    """


class Mutation(communication.schema.Mutation, customer.schema.Mutation, graphene.ObjectType):
    """
    # This class will inherit from multiple Mutation
    """


schema = graphene.Schema(query=Query, mutation=Mutation)
