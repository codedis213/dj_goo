import dateutil.parser as parser
from django import template
from django.utils.safestring import mark_safe

register = template.Library()


class FormatTimeNode(template.Node):
    """
    class to format Time node
    """
    def __init__(self, date_to_be_formatted, format_string):
        """

        :param date_to_be_formatted:
        :param format_string:
        """
        self.date_to_be_formatted = template.Variable(date_to_be_formatted)
        self.format_string = format_string

    def render(self, context):
        """
        get the date, format it into the given format
        :param context:
        :return:
        """
        try:
            actual_date = self.date_to_be_formatted.resolve(context)
            actual_date = parser.parse(actual_date)
            return actual_date.strftime(self.format_string)
        except template.VariableDoesNotExist:
            return ''


def do_format_time(parser, token):
    """
    format time
    :param parser:
    :param token:
    :return:
    """
    try:
        # split_contents() knows not to split quoted strings.
        tag_name, date_to_be_formatted, format_string = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError(
            "%r tag requires exactly two arguments" % token.contents.split()[0]
        )
    if not (format_string[0] == format_string[-1] and
            format_string[0] in ('"', "'")):
        raise template.TemplateSyntaxError(
            "%r tag's argument should be in quotes" % tag_name
        )
    return FormatTimeNode(date_to_be_formatted, format_string[1:-1])


@register.simple_tag
def set_review_star(*args, **kwargs):
    """
    set the stars into the review
    :param args:
    :param kwargs:
    :return:
    """
    rating = kwargs["rating"]

    span = ''

    for _ in range(rating):
        span += '<span class="fa fa-star checked"></span>'

    for _ in range(5 - rating):
        span += '<span class="fa fa-star"></span>'

    return mark_safe(span)


@register.simple_tag
def review_score(*args, **kwargs):
    """
    create a dictionary containing key and value like below
    star_dict = {5: 20, 4: 5, 3: 2, 2: 0, 1: 0}
    where key 5 means 5-start
    and value 20 means number of stars as 5 stars

    :param args:
    :param kwargs:
    :return:
    """
    review_on_product = kwargs["reviewOnProduct_obj"]
    star_dict = {5: 0, 4: 0, 3: 0, 2: 0, 1: 0}

    for review in review_on_product:
        star_dict[review.get("rating")] += 1

    sum_value = sum(star_dict.values())
    star_dict["avg_rating"] = 0

    if sum_value:
        avg_rating = (5 * star_dict[5] +
                      4 * star_dict[4] +
                      3 * star_dict[3] +
                      2 * star_dict[2] +
                      1 * star_dict[1]) / sum_value
        star_dict["avg_rating"] = "{0:.2f}".format(avg_rating)

    return star_dict


register.tag('format_time', do_format_time)
