from django.contrib import admin

from .models import Voucher, Gift, VoucherAppliedOnProduct, \
    VoucherAppliedOnCategory, VoucherAppliedOnBrand


class VoucherAppliedOnProductInline(admin.TabularInline):
    """
    TabularInline Admin form for VoucherAppliedOnProduct
    """
    model = VoucherAppliedOnProduct
    extra = 1


class VoucherAppliedOnCategoryInline(admin.TabularInline):
    """
   TabularInline Admin form for VoucherAppliedOnCategory
   """
    model = VoucherAppliedOnCategory
    extra = 1


class VoucherAppliedOnBrandInline(admin.TabularInline):
    """
   TabularInline Admin form for VoucherAppliedOnBrand
   """
    model = VoucherAppliedOnBrand
    extra = 1


class VoucherAdmin(admin.ModelAdmin):
    """
   ModelAdmin for VoucherAdmin
   """
    inlines = (VoucherAppliedOnProductInline, VoucherAppliedOnCategoryInline,
               VoucherAppliedOnBrandInline)


admin.site.register(Voucher, VoucherAdmin)
admin.site.register(Gift)
