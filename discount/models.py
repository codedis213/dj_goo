from djongo import models

from donnyji.donnyji_constants import DISCOUNT_TYPE, VOUCHER_TYPE
from product.models import Product, Category, Brand


class VoucherAppliedOnProduct(models.Model):
    """
    Model for Voucher Applied On Product
    """
    voucher = models.ForeignKey("Voucher",
                                related_name="voucher_on_product",
                                on_delete=models.DO_NOTHING)
    product = models.ForeignKey("product.Product",
                                related_name="product_under_voucher",
                                on_delete=models.DO_NOTHING)

    discount_type = models.IntegerField(choices=DISCOUNT_TYPE, default=1)
    discount_value = models.FloatField()


class VoucherAppliedOnCategory(models.Model):
    """
    Model for Voucher Applied On Category
    """
    voucher = models.ForeignKey("Voucher",
                                related_name="voucher_on_category",
                                on_delete=models.DO_NOTHING)
    category = models.ForeignKey("product.Category",
                                 related_name="category_under_voucher",
                                 on_delete=models.DO_NOTHING)

    discount_type = models.IntegerField(choices=DISCOUNT_TYPE, default=1)
    discount_value = models.FloatField()


class VoucherAppliedOnBrand(models.Model):
    """
    Model for Voucher Applied On Brand
    """
    voucher = models.ForeignKey("Voucher",
                                related_name="voucher_on_brand",
                                on_delete=models.DO_NOTHING)
    brand = models.ForeignKey("product.Brand",
                              related_name="brand_under_voucher",
                              on_delete=models.DO_NOTHING)

    discount_type = models.IntegerField(choices=DISCOUNT_TYPE, default=1)
    discount_value = models.FloatField()


class Voucher(models.Model):
    """
    Model for Voucher
    """
    added_by = models.ForeignKey("store.StoreHouse",
                                 related_name="voucher_added_by",
                                 on_delete=models.DO_NOTHING)
    voucher_type = models.IntegerField(choices=VOUCHER_TYPE, default=1)
    voucher_name = models.CharField(max_length=15)
    voucher_description = models.TextField(blank=True, null=True)
    is_active = models.BooleanField()
    expire_time = models.DateTimeField()


class Gift(models.Model):
    """
    Model for Gift
    """
    gift_code = models.CharField(max_length=15)
    is_active = models.BooleanField()
    expire_time = models.DateTimeField()
    applied_to_product = models.ManyToManyField(
        Product,
        related_name="gift_applied_to_product")
    applied_to_cat = models.ManyToManyField(
        Category,
        related_name="gift_applied_to_cat")
    applied_to_brand = models.ManyToManyField(
        Brand,
        related_name="gift_applied_to_brand")
