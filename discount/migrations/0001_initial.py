# Generated by Django 2.2.5 on 2020-01-06 12:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('product', '0001_initial'),
        ('store', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Voucher',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('voucher_type', models.IntegerField(choices=[(1, 'Discount'), (2, 'Sale')], default=1)),
                ('voucher_name', models.CharField(max_length=15)),
                ('voucher_description', models.TextField(blank=True, null=True)),
                ('is_active', models.BooleanField()),
                ('expire_time', models.DateTimeField()),
                ('added_by', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='voucher_added_by', to='store.StoreHouse')),
            ],
        ),
        migrations.CreateModel(
            name='VoucherAppliedOnProduct',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('discount_type', models.IntegerField(choices=[(1, 'Percentage'), (2, 'Flat')], default=1)),
                ('discount_value', models.FloatField()),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='product_under_voucher', to='product.Product')),
                ('voucher', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='voucher_on_product', to='discount.Voucher')),
            ],
        ),
        migrations.CreateModel(
            name='VoucherAppliedOnCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('discount_type', models.IntegerField(choices=[(1, 'Percentage'), (2, 'Flat')], default=1)),
                ('discount_value', models.FloatField()),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='category_under_voucher', to='product.Category')),
                ('voucher', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='voucher_on_category', to='discount.Voucher')),
            ],
        ),
        migrations.CreateModel(
            name='VoucherAppliedOnBrand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('discount_type', models.IntegerField(choices=[(1, 'Percentage'), (2, 'Flat')], default=1)),
                ('discount_value', models.FloatField()),
                ('brand', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='brand_under_voucher', to='product.Brand')),
                ('voucher', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='voucher_on_brand', to='discount.Voucher')),
            ],
        ),
        migrations.CreateModel(
            name='Gift',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gift_code', models.CharField(max_length=15)),
                ('is_active', models.BooleanField()),
                ('expire_time', models.DateTimeField()),
                ('applied_to_brand', models.ManyToManyField(related_name='gift_applied_to_brand', to='product.Brand')),
                ('applied_to_cat', models.ManyToManyField(related_name='gift_applied_to_cat', to='product.Category')),
                ('applied_to_product', models.ManyToManyField(related_name='gift_applied_to_product', to='product.Product')),
            ],
        ),
    ]
