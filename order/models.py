from djongo import models

from discount.models import Voucher, Gift
from donnyji.donnyji_constants import ORDER_TYPE_CHOICES, \
    CANCEL_OR_RETURN_CHOICE
from payment.models import Payment
from product.models import Product


class ProductOrdered(models.Model):
    """
    Model for Product Order
    """
    ordered_bag = models.ForeignKey(
        "OrderedBag",
        related_name="ordered_bag",
        on_delete=models.DO_NOTHING)
    product_detail = models.ForeignKey(
        Product,
        related_name="po_product_detail",
        on_delete=models.DO_NOTHING)
    is_cancel_or_return = models.IntegerField(choices=CANCEL_OR_RETURN_CHOICE,
                                              default=1)
    order_type = models.IntegerField(choices=ORDER_TYPE_CHOICES, default=1)


class VoucherAppliedOnOrder(models.Model):
    """
    Model for Voucher Applied On Order
    """
    ordered_bag = models.ForeignKey(
        "OrderedBag",
        related_name="ordered_bag_voucher_applied",
        on_delete=models.DO_NOTHING)

    voucher_applied = models.ForeignKey(Voucher,
                                        related_name="voucher_applied",
                                        on_delete=models.DO_NOTHING)


class GiftAppliedOnOrder(models.Model):
    """
    Model for Gift Applied On Order
    """

    ordered_bag = models.ForeignKey(
        "OrderedBag",
        related_name="ordered_bag_gift_applied",
        on_delete=models.DO_NOTHING)

    gift_applied = models.ForeignKey(Gift,
                                     related_name="gift_applied",
                                     on_delete=models.DO_NOTHING)


class OrderedBag(models.Model):
    """
    Model for Ordered Bag
    """

    customer = models.ForeignKey("account.User",
                                 related_name="customer_ordered",
                                 on_delete=models.DO_NOTHING)
    calculated_tax = models.FloatField()
    total_discount = models.FloatField()
    shipping_fees = models.FloatField()
    actual_price = models.FloatField()
    sale_price = models.FloatField()

    payment_track = models.ForeignKey(
        Payment,
        related_name="order_payment_track",
        on_delete=models.CASCADE)
