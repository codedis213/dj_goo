from django.contrib import admin

from .models import ProductOrdered, OrderedBag, VoucherAppliedOnOrder, \
    GiftAppliedOnOrder


class ProductOrderedInline(admin.TabularInline):
    """
    TabularInline Admin form for ProductOrdered
    """
    model = ProductOrdered
    extra = 1


class VoucherAppliedOnOrderInline(admin.TabularInline):
    """
    TabularInline Admin form for VoucherAppliedOnOrder
    """
    model = VoucherAppliedOnOrder
    extra = 1


class GiftAppliedOnOrderInline(admin.TabularInline):
    """
    TabularInline Admin form for GiftAppliedOnOrder
    """
    model = GiftAppliedOnOrder
    extra = 1


class OrderedBagAdmin(admin.ModelAdmin):
    """
    ModelAdmin  for OrderedBag
    """
    inlines = (ProductOrderedInline, VoucherAppliedOnOrderInline,
               GiftAppliedOnOrderInline)


class ProductOrderedAdmin(admin.ModelAdmin):
    """
    ModelAdmin  for ProductOrdered
    """

    def get_model_perms(self, request):
        """
        :param request:
        :return:
        """
        perms = admin.ModelAdmin.get_model_perms(self, request)
        perms['list_hide'] = True
        return perms


admin.site.register(ProductOrdered, ProductOrderedAdmin)
admin.site.register(OrderedBag, OrderedBagAdmin)
