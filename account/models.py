# from django.contrib.postgres.fields import ArrayField
import jsonfield
from django.contrib.auth.models import AbstractUser
from djongo import models
from phonenumber_field.modelfields import PhoneNumberField


class User(AbstractUser):
    """
    Users within the Django authentication system are represented by this
    model.

    Username, password and email are required. Other fields are optional.
    """
    is_customer = models.BooleanField(default=False)

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'


class OtherPhone(models.Model):
    """
    Other Phone
    """
    user = models.ForeignKey(User, related_name="user_phone_number",
                             on_delete=models.CASCADE)
    phone_number = PhoneNumberField()
    is_primary = models.BooleanField(default=False)


class UserSession(models.Model):
    """
    User Session Model
    """
    url_hit = models.URLField()
    request_data = jsonfield.JSONField()
    comments = models.TextField()
    country = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    area = models.CharField(max_length=50, blank=True, null=True)
    near_by = models.CharField(max_length=50, blank=True, null=True)
    radius_set = models.CharField(max_length=10, blank=True, null=True)
    ip_address = models.CharField(max_length=30)
    latitude_set = models.CharField(max_length=15, blank=True, null=True)
    longitude_set = models.CharField(max_length=15, blank=True, null=True)
    response_data = jsonfield.JSONField()
    response_status = models.CharField(max_length=10)


class CardDetail(models.Model):
    """
    Card Detail Model
    """
    bank_name = models.CharField(max_length=50)
    card_number = models.CharField(max_length=50)
    card_holder = models.CharField(max_length=50)
    card_csv = models.CharField(max_length=10)
    card_expiry = models.CharField(max_length=10)
