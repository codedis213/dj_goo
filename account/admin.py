from django.contrib import admin
from .models import CardDetail, OtherPhone, User


class OtherPhoneInline(admin.TabularInline):
    """
    Inline Other Phone
    """
    model = OtherPhone
    extra = 1


class UserAdmin(admin.ModelAdmin):
    """
    User admin class
    """
    search_fields = ('username',)


admin.site.register(CardDetail)
admin.site.register(User, UserAdmin)
