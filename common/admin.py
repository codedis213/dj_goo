from django.contrib import admin

from .models import Images, Address

admin.site.register(Images)
admin.site.register(Address)
