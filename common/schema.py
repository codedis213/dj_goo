import graphene
from graphene_django.types import DjangoObjectType

from account.models import User


class UserType(DjangoObjectType):
    """
    GraphQL Schema forUser Model
    """

    class Meta:
        """
        Meta for GraphQL UserType
        """
        model = User


class Query:
    """
    GraphQl Query for users
    """
    all_users = graphene.List(UserType)

    def resolve_all(self, info, **kwargs):
        """
        :param info:
        :param kwargs:
        :return:
        """
        user_obj = User.objects.all()[:100]
        return user_obj
