from djongo import models


class Images(models.Model):
    """
    Model Image
    """
    image = models.ImageField(upload_to='images', null=True,
                              default='profile_image/none/no-img.png')
    image_string = models.URLField(null=True, blank=True)


class Address(models.Model):
    """
    Address Model
    """
    country = models.CharField(max_length=30)
    state = models.CharField(max_length=30)
    city = models.CharField(max_length=30)
    area = models.CharField(max_length=30, blank=True, null=True)
    zip = models.CharField(max_length=30)
    near_by = models.CharField(max_length=50, blank=True, null=True)
    search_address = models.TextField(null=True, blank=True)


class OtherImages(models.Model):
    """
    Other Images Model
    """
    product = models.ForeignKey("product.Product",
                                related_name="o_img_product",
                                on_delete=models.CASCADE)
    image = models.ForeignKey("Images", related_name="o_img_image",
                              on_delete=models.CASCADE)
