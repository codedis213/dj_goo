# from django.contrib.postgres.fields import ArrayField
from djongo import models
from phonenumber_field.modelfields import PhoneNumberField

from account.models import User


class IdImages(models.Model):
    """
    Model for Id proof of Images
    """
    seller = models.ForeignKey("Seller", related_name="seller_id_images",
                               on_delete=models.DO_NOTHING)
    image = models.ImageField(upload_to='images', null=True,
                              default='profile_image/none/no-img.png')


class SellerPhone(models.Model):
    """
    Model for Seller Phone
    """
    seller = models.ForeignKey("Seller", related_name="seller_phone_number",
                               on_delete=models.DO_NOTHING)
    phone_number = PhoneNumberField()
    is_primary = models.BooleanField(default=False)


class Seller(models.Model):
    """
    Model Seller
    """
    seller = models.ForeignKey(
        User, related_name="slr_seller",
        on_delete=models.CASCADE)
    middle_name = models.CharField(max_length=50, null=True, blank=True)
    id_proof_no = models.CharField(max_length=50)
    id_seller_approved = models.BooleanField(default=False)


class SellerAddressLatLon(models.Model):
    """
    Model for Seller Address Lat Lon
    """
    category = models.CharField(max_length=100, null=True, blank=True)
    seller_url = models.TextField(null=True, blank=True)
    seller_name = models.CharField(max_length=100)
    latitude = models.CharField(max_length=50, blank=True, null=True)
    longitude = models.CharField(max_length=50, blank=True, null=True)
    lat_lon_z = models.CharField(max_length=50, blank=True, null=True)
    full_address = models.TextField(null=True, blank=True)
    contact_number = models.CharField(max_length=50, null=True, blank=True)
    seller_rating = models.CharField(max_length=50, null=True, blank=True)

    def __repr__(self):
        """
        override the __repr__
        :return:
        """
        return '{}'.format(self.seller_name)
