from django.contrib import admin

from store.forms import StoreHouseForm
from store.models import StoreHouse
from .models import Seller, SellerPhone, IdImages


class SellerPhoneInline(admin.TabularInline):
    """
    TabularInline Admin form for Seller Phone
    """
    model = SellerPhone
    extra = 1


class IdImagesInline(admin.TabularInline):
    """
    TabularInline Admin form for IdImages
    """
    model = IdImages
    extra = 1


class StoreHouseInline(admin.TabularInline):
    """
    TabularInline Admin form for StoreHouse
    """
    model = StoreHouse
    extra = 1
    form = StoreHouseForm


class SellerAdmin(admin.ModelAdmin):
    """
    ModelAdmin for Seller
    """
    search_fields = ['seller__username']
    inlines = (SellerPhoneInline, IdImagesInline,
               StoreHouseInline)


admin.site.register(Seller, SellerAdmin)
